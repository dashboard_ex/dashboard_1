let header_item_button = document.getElementsByClassName("header-item-button");
let body_item = document.getElementsByClassName("body-item");

for(let i = 0; i < header_item_button.length; i++){
	body_item[i].style.bottom = -(body_item[i].offsetHeight + header_item_button[i].offsetHeight) + "px";
	header_item_button[i].addEventListener("click", function () {
		let reset = header_item_button[i].classList.contains("active");
		for (let j = 0; j < header_item_button.length; j++){
			header_item_button[j].classList.remove("active");
			header_item_button[j].style.bottom = 0;
			body_item[j].style.bottom = -(body_item[j].offsetHeight + header_item_button[j].offsetHeight) + "px";
		}
		if(reset){
			header_item_button[i].style.bottom = 0;
			body_item[i].style.bottom = -(body_item[i].offsetHeight + header_item_button[i].offsetHeight) + "px";
		} else {
			header_item_button[i].classList.add("active");
			header_item_button[i].style.bottom = document.getElementsByClassName("header").item(0).offsetHeight - header_item_button[i].offsetHeight + "px";
			body_item[i].style.bottom = 0;
		}
	});
}

